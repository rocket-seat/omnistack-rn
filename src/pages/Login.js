import React, { Component } from 'react'
import { Text, View, KeyboardAvoidingView, TextInput, TouchableOpacity, AsyncStorage } from 'react-native'
import Reactotron from 'reactotron-react-native'

import styles from './LoginCss'
import Icon from 'react-native-vector-icons/FontAwesome'

class Login extends Component {
    state = {
        username: ''
    }

    async componentDidMount() {
        const username = await AsyncStorage.getItem("@GoTweet:username")

        if (username) {
            this.props.navigation.navigate('App')
        }
    }

    handleLogin = async (e) => {
        const { username } = this.state

        if (!username) {
            return;
        }

        Reactotron.log({ 'username': username });

        await AsyncStorage.setItem("@GoTweet:username", username)

        this.props.navigation.navigate('App')
    }

    handleInput = (username) => {
        this.setState({ username })
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View style={styles.content}>
                    <View>
                        <Icon name="twitter" size={64} color="#4BB0EE" />
                    </View>

                    <TextInput style={styles.input} placeholder="Nome do usuário" returnKeyType="send"
                        value={this.state.username}
                        onSubmitEditing={(e) => this.handleLogin(e)} onChangeText={(e) => this.handleInput(e)}
                    />

                    <TouchableOpacity onPress={() => { }} style={styles.button}>
                        <Text>Entrar</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default Login