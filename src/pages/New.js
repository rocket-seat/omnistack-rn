import React, { Component } from 'react'
import { Text, View, SafeAreaView, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'

import api from '../services/api'
import styles from './NewCss'
import Icon from 'react-native-vector-icons/MaterialIcons'

class New extends Component {
    static navigationOptions = {
        header: null
    }

    state = {
        newTweet: ''
    }

    goBack = () => {
        this.props.navigation.pop()
    }

    handleInput = (newTweet) => {
        this.setState({ newTweet })
    }

    handleTweet = async () => {
        const content = this.state.newTweet
        const author = await AsyncStorage.getItem("@GoTweet:username")

        await api.post('tweets', { author, content })
        this.goBack()
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={this.goBack}>
                        <Icon name="close" size={24} color="#4BB0EE" />
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.button} onPress={this.handleTweet}>
                        <Text style={styles.buttonText}>Tweetar</Text>
                    </TouchableOpacity>

                    <TextInput style={styles.input} multiline placeholder="O que está acontecendo?" placeholderTextColor="#999" value={this.state.newTweet} returnKeyType="send" onChangeText={this.handleInput} onSubmitEditing={this.handleInput} />
                </View>
            </SafeAreaView>
        )
    }
}

export default New