import React, { Component } from 'react'
import { Text, View, FlatList, TouchableOpacity, Platform } from 'react-native'
import Reactotron from 'reactotron-react-native'
import api from '../services/api'

import socket from 'socket.io-client/dist/socket.io.slim.dev'
import Tweet from '../components/Tweet'
import styles from './TimeLineCss'
import Icon from 'react-native-vector-icons/MaterialIcons'

class Timeline extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'Inicio',
        headerRight: (
            <TouchableOpacity onPress={() => navigation.navigate('NewTweet')}>
                <Icon name="add-circle-outline" size={24} color="#4BB0EE" style={{ marginRight: 20 }} />
            </TouchableOpacity>
        )
    })

    state = {
        tweets: []
    }

    async componentDidMount() {
        this.subscribeToEvents();

        const res = await api.get('tweets')

        this.setState({ tweets: res.data })
    }

    subscribeToEvents = () => {
        const localURL = Platform.OS === 'android' ? '10.0.3.2' : 'localhost'

        const io = socket('http://' + localURL + ':3000')
        Reactotron.log('subscribeToEvents')

        io.on('novoTweet', data => {
            Reactotron.log({ 'novoTweet': data })

            this.setState({
                tweets: [data, ...this.state.tweets]
            })
        })

        io.on('novoLike', data => {
            Reactotron.log({ 'novoLike': data })

            this.setState({
                tweets: this.state.tweets.map(t => t._id == data._id ? data : t)
            })

            // Envia algo ao servidor
            // io.broadcast.emit('message', message);
            // io.emit('channel1', 'Hi server');
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList data={this.state.tweets} keyExtractor={(t) => t._id} renderItem={({ item }) => <Tweet tweet={item} />} />
            </View>
        )
    }
}

export default Timeline