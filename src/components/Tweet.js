import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'

import api from '../services/api'
import styles from './TweetCss'
import Icon from 'react-native-vector-icons/Ionicons'

class Tweet extends Component {

    handleLike = () => {
        const { _id } = this.props.tweet
        api.post('likes/' + _id)
    }

    render() {
        const { tweet } = this.props;

        return (
            <View style={styles.container}>
                <Text style={styles.author}>{tweet.author}</Text>
                <Text style={styles.content}>{tweet.content}</Text>

                <TouchableOpacity onPress={this.handleLike} style={styles.likeButton}>
                    <Icon name="ios-heart-empty" size={20} color="#999" />
                    <Text style={styles.likeText}>{tweet.likes}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default Tweet