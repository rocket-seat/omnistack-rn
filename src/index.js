import React from 'react'
import Routes from './routes'

import Reactotron from 'reactotron-react-native'

Reactotron.configure({ host: '10.10.20.215', name: 'goWeek' }).useReactNative().connect()

Reactotron.warn('Iniciando aplicação')

const App = () => <Routes />

export default App